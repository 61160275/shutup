import {AUTH_LOGIN,AUTH_LOGOUT} from '../mutation-types'
export default {
    namespaced: true,
    state: () =>({
        user: null,
    }),
    mutations: {
        [AUTH_LOGIN](state, payload){
          state.user = payload
        },
        [AUTH_LOGOUT] (state){
          state.user = null
        }
      },
      actions: {
        login ({commit}, payload){
            const user = { name: 'Palapol', email: 'Palapol@user.com'}
          commit(AUTH_LOGIN, user)
        },
        logout ({commit}){
           commit(AUTH_LOGOUT)
        }
      },
      getters: { 
          isLogin (state, getters){
            return state.user != null
          }
      }
}