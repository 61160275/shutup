const mongoose = require('mongoose')
const { Schema } = mongoose
const documentSchema = Schema({
    user_id: String,
    filename: String,
    path: String,
    date: Date,
    status:String,
    type:String,

})

module.exports = mongoose.model('Document',documentSchema)