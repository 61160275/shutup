const { ROLE } = require('../constant.js')
const mongoose = require('mongoose')
const { Schema } = mongoose
const userSchema = Schema({
    email: String,
    password: String,
    roles:{
        type: [String],
        default: [ROLE.STUDENT]
    },
    name: String,
    type: String,
    branch: String,
    studentId: String,

})

module.exports = mongoose.model('User',userSchema)