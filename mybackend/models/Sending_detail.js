const mongoose = require('mongoose')
const { Schema } = mongoose
const sending_detailSchema = Schema({
    doc_id: String,
    stu_id: String,
    tec_id: String,
    note: String

})

module.exports = mongoose.model('Sending_detail',sending_detailSchema)