const express = require('express')
const { reset } = require('nodemon')
const router = express.Router()
const Sending_detail = require('../models/Sending_detail')
const Document = require('../models/Document')
const User = require('../models/User')

const sending_details = [
    // {
    //     id:'001',
    //     doc_id:'001',
    //     stu_id:'005',
    //     tec_id:'002',
    //     note:'can you sign this file pls'
    // },
    // {
    //     id:'002',
    //     doc_id:'002',
    //     stu_id:'005',
    //     tec_id:'003',
    //     note:'can you sign this file pls'
    // }
]
const getSending_details = async function (req,res,next){
    try{
        const sending_details = await Sending_detail.find({}).then()
        res.json(sending_details)
      } catch(err){
        return res.status(500).send({ 
          message: err.message
        })
      }
}

const getSending_detail = async function(req,res,next){
    const id = req.params.id
    try{
        const sending_detail = await Sending_detail.find({tec_id:id}).exec()
        var sendinglist = []
        for (i = 0; i < sending_detail.length; i++){
            var detail = {}
            const document = await Document.findOne({_id: sending_detail[i].doc_id})
            const student = await User.findOne({_id: sending_detail[i].stu_id})
            detail.name = student.name
            detail.studentId = student.studentId
            detail.doc_id = sending_detail[i].doc_id
            detail.filename = document.filename
            detail.path = document.path
            detail.doc_type = document.type
            detail.note = sending_detail[i].note

            sendinglist.push(detail)
            console.log(detail)
        }
        res.json(sendinglist)
        if (sendinglist==null){
            return res.status(404).json({
                message: 'Sending_detail not found'
            })
        }
    }catch (err){ 
        res.status(404).json({
            message: err.message
        })
    }
    
}
const addSending_details = async function (req,res,next){
    const newSending_detail = new Sending_detail({
        doc_id: req.body.doc_id,
        stu_id: req.body.stu_id,
        tec_id: req.body.tec_id,
        note: req.body.note,
    })
    try {
        await newSending_detail.save()
        res.status(201).json(newSending_detail)  
    }catch (err){
        return res.status(201).send({
          message: err.message
        })
    }
}

const updateSending_detail = async function (req,res,next){
    const sending_detailId = req.params.id
    try{
        const sending_detail = await Sending_detail.findById(sending_detailId)
        sending_detail.doc_id= req.body.doc_id,
        sending_detail.stu_id= req.body.stu_id,
        sending_detail.tec_id= req.body.tec_id,
        sending_detail.note= req.body.note,
        await sending_detail.save()
        return res.status(200).json(sending_detail)
    }catch (err){
        return res.status(404).send({message: err.message})
    }
}

const deleteSending_detail = async function (req,res,next){
    const sending_detailId = req.params.id
    try{
        await Sending_detail.findByIdAndDelete(sending_detailId)
        return res.status(200).send()
    }catch (err){
        return res.status(404).send({message: err.message})
    }
}

router.get('/', getSending_details)
router.get('/:id', getSending_detail)
router.post('/', addSending_details)
router.put('/:id', updateSending_detail)
router.delete('/:id', deleteSending_detail)

  
module.exports = router