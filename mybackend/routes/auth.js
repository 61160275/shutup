const express = require('express')
const { reset } = require('nodemon')
const router = express.Router()
const User = require('../models/User')
const { generateAccessToken } = require('../helpers/auth')


const login = async function(req,res,next){
    const email = req.body.email
    const password = req.body.password

      try{
          const user = await User.findOne({ email: email, password: password},'-password').exec()
          if (user==null){
              return res.status(404).json({
                  message: 'user not found'
              })
          }

          const token = generateAccessToken({email: user.email , roles: user.roles})
          res.json({ user: user , token: token})
      }catch (err){
        return res.status(404).json({
              message: err.message
          })
      }
    
  }

router.post('/login', login)
module.exports = router
