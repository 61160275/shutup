const ROLE = {
    STUDENT: 'STUDENT',
    TEACHER: 'TEACHER',
    OFFICER: 'OFFICER'
}

module.exports = {
  ROLE
}
  
